'use strict'

// load the options object, which is used for configs from the user
const opts = require('./lib/options')

// export function that allows the user to provide options - usage will be require('indexed-search')({/* options */})
module.exports = userOptions => {
  Object.keys(userOptions).forEach(key => opts[key] = userOptions[key]) // apply user options to the options object
  return require('./lib/build-index') // once options have been provided, export the buildIndex function
}