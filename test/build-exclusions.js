'use strict'

const expect = require('chai').expect
const rewire = require('rewire')
const build = rewire('../lib/build-exclusions')

describe('build-exclusions', () => {

  before(() => { // build the directory structure in the temp directory
    
  })

  after(() => { // clean up directory structure
    
  })

  it('should check if a path is a directory')
  
  it('should list all the files in the directory')

  it('should list the words in a file')

  it('should create a distinct array from a string array', () => {
    const distinctStringArray = build.__get__('distinctStringArray')
    const distinct = [ 'a', 'b', 'c' ], start = ['a', 'a', 'b', 'c', 'c', 'b', 'b', 'a', 'b', 'c' ]
    const processed = distinctStringArray(start)
    expect(processed).to.eql(distinct)
  })

  it('should append to the word count object from file contents')

  it('should build an exclusions list from a folder')

  it('should build an exclusions list from a file')

  it('should recursively build an exclusions list from a folder')
  
})