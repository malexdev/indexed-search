'use strict'

const path = require('path')
const os = require('os')
const Promise = require('bluebird')
const fs = Promise.promisifyAll(require('fs'))

const options = {

  // allow the user to provide a custom path to store index files in
  // defaults to /tmp/indexed-search/
  indexStoragePath: path.join(os.tmpdir(), 'indexed-search'),

  // allow the user to provide a custom exclusions list
  // if this is provided and dynamic exclusions are still allowed,
  // both will be applied.
  exclusions: [],

  // allow the user to specify whether we should build a dynamic exclusion list
  dynamicExclusions: true,

  // allow user to specify what percentage a word needs to appear in
  // to consider it part of the dynamic exclusion list
  // should be a value between 0 and 1
  dynamicExclusionThreshold: 0.75,

  // allow the user to specify a file name filter
  // will be passed the full path to the file
  // should synchronously return true if the file should be parsed
  fileNameFilter: () => true,

  // allow the user to specify a file reader
  // will be passed the full path to the file
  // should return a promise that resolves to the file contents
  fileReader: path => fs.readFileAsync(path, 'utf8'),

}

module.exports = options