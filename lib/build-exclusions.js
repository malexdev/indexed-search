'use strict'

const Promise = require('bluebird')
const fs = Promise.promisifyAll(require('fs'))
const path = require('path')
const opts = require('./options')

const wordFileCounts = {}
let totalFileCount = 0

// recurse through all the files in this folder
// keep a list of each word, along with how many files that word appears in
// if a word appears in more than opts.dynamicExclusionThreshold percentage of the files,
// it is considered something that should be excluded
function buildExclusions(rootPath) {
  return isDirectory(rootPath).then(directory => {
    if (directory) {
      return filesInDirectory(rootPath)
      .then(filePaths => Promise.map(filePaths, buildExclusions))
    } else {
      totalFileCount++
      return wordsInFile(rootPath).then(appendWordCount)
    }
  })

  // at this point the wordFileCounts has been built
  // so return an array of words where the count is over opts.dynamicExclusionThreshold percentage of the files
  .then(() => {
    const minExcludeThreshold = totalFileCount * opts.dynamicExclusionThreshold
    return Object.keys(wordFileCounts).filter(word => wordFileCounts[word] >= minExcludeThreshold)
  })
}

// stat the path, then return whether the path is a directory
function isDirectory(filePath) {
  return fs.statAsync(filePath).then(stat => stat.isDirectory()) 
}

// read the files in the directory
// map all the file names into their full paths
// filter the list by the given fileNameFilter function
function filesInDirectory(dirPath) {
  return fs.readdirAsync(dirPath)
  .then(fileNames => fileNames.map(name => path.join(dirPath, name)))
  .then(filePaths => filePaths.filter(opts.fileNameFilter))
}

// read the file
// split the file into words
// then make a distinct list from the words
function wordsInFile(filePath) {
  return opts.fileReader(filePath) 
  .then(content => content.split(/\s+/))
  .then(distinctStringArray)
}

// take an array of strings and return a distinct array of strings
// uses an object for the distinct test so that the speed remains constant
function distinctStringArray(arrayOfStrings) {
  const wordList = {}
  arrayOfStrings.forEach(str => wordList[str] = true)
  return Object.keys(wordList)
}

// this function is passed an array of words that were in a given file
// for each word in the array, increment the wordFileCounts key for that word
// or create if not exists
function appendWordCount(words) {
  words.forEach(word => {
    if (wordFileCounts[word]) {
      wordFileCounts[word]++
    } else {
      wordFileCounts[word] = 1
    }
  })
}

module.exports = buildExclusions